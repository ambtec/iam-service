#!/bin/bash

# generates markdown file out of the asyncapi doc

# ensure the generator is installed (https://github.com/asyncapi/generator)
ag api-doc-asyncapi.yml @asyncapi/markdown-template -o api-doc --force-write

