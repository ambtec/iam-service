#syntax=docker/dockerfile:experimental

# build kafka module
FROM maven:3.8.1-openjdk-16-slim as KAFKA_MODULE_BUILD

COPY pom.xml /tmp/
COPY keycloak-kafka-module/pom.xml /tmp/keycloak-kafka-module/pom.xml
COPY keycloak-kafka-module/src /tmp/keycloak-kafka-module/src

WORKDIR /tmp/

RUN --mount=type=cache,target=/root/.m2 mvn clean package -DskipTests

# build keycloak image
FROM jboss/keycloak:15.0.2

ENV KEYCLOAK_IMPORT /opt/jboss/keycloak/imports/realm-export.json

COPY ./iam-service/themes/ambtec /opt/jboss/keycloak/themes/ambtec
COPY ./iam-service/export/realm-export.json /opt/jboss/keycloak/imports/realm-export.json

# kafka module START
COPY --from=KAFKA_MODULE_BUILD /tmp/keycloak-kafka-module/target/keycloak-kafka-module-0.0.1-SNAPSHOT-jar-with-dependencies.jar /opt/jboss/keycloak/standalone/deployments/
ADD add-kafka-config.cli /opt/jboss/startup-scripts/
# kafka module END

EXPOSE ${PORT}
EXPOSE 8080
EXPOSE 8443

CMD ["-b", "0.0.0.0", "-Djboss.http.port=80"]
