# IAM Service Keycloak 1.0.0 documentation

* Default content type: [application/json](https://www.iana.org/assignments/media-types/application/json)

This service consumes internally keycloak events (as extension) and publishes the event with some additional data to kafka

## Table of Contents

* [Operations](#operations)
  * [PUB iam-user-events](#pub-iam-user-events-operation)

## Operations

### PUB `iam-user-events` Operation

#### Message `UserEvent`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | object | - | - | - | **additional properties are allowed** |
| id | array<any> | id of the event | - | - | - |
| time | integer | when the event occurred | - | - | - |
| type | string | type of event (e.g. LOGIN, LOGOUT) | - | - | - |
| realmId | string | which realm the event belongs to | - | - | - |
| clientId | string | id of the used iam client | - | - | - |
| userId | string | which user the event belongs to | - | - | - |
| sessionId | string | tbd | - | - | - |
| ipAdress | string | tbd | - | - | - |
| error | string | tbd | - | - | - |
| details | object | additional information as generic key value map | - | - | - |
| details (additional properties) | string | - | - | - | - |
| custom | object | custom properties that not belongs to original event (calculated) | - | - | **additional properties are allowed** |
| custom.resourceAccess | string | - | - | - | - |

> Examples of payload _(generated)_

```json
{
  "id": "546de9bb-9ae0-4a17-9a5b-4e30346c5467",
  "time": 1640209498678,
  "type": "LOGIN",
  "realmId": "ambtec",
  "clientId": "testclient",
  "userId": "08e2831e-f24e-4a65-9a60-f62d2d5e3eda",
  "sessionId": "ca5b9f21-bb28-4871-b8d4-2c96cc0e9362",
  "ipAddress": "172.25.0.1",
  "details": {
    "auth_method": "openid-connect",
    "token_id": "e364c271-e865-498f-89d6-30af88a1cadc",
    "grant_type": "password",
    "refresh_token_type": "Refresh",
    "scope": "email profile",
    "refresh_token_id": "2b2fa9be-45a5-4aad-b7fd-9dd3363fe69b",
    "client_auth_method": "client-secret",
    "username": "testuser"
  }
}
```



