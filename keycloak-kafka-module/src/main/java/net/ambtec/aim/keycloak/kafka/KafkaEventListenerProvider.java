package net.ambtec.aim.keycloak.kafka;

import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;

public class KafkaEventListenerProvider implements EventListenerProvider {

    private final KeycloakEventProcessor eventProcessor;

    public KafkaEventListenerProvider(ModuleConfig moduleConfig, KeycloakSession session) {
        eventProcessor = new KeycloakEventProcessor(moduleConfig,
                session,
                KafkaProducerFactory.createProducer(moduleConfig.getKafkaProducerProperties()));
    }

    @Override
    public void onEvent(Event event) {
        eventProcessor.processUserEvent(event);
    }

    @Override
    public void onEvent(AdminEvent event, boolean includeRepresentation) {
        eventProcessor.processAdminEvent(event);
    }

    @Override
    public void close() {
        // ignore
    }
}
