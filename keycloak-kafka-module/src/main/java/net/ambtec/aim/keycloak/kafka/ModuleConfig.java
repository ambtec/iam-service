package net.ambtec.aim.keycloak.kafka;

import java.util.Map;

public class ModuleConfig {
    private final String topicUserEvents;
    private final String topicAdminEvents;
    private final String topicDashboardEvents;
    private final Map<String, Object> kafkaProducerProperties;

    public ModuleConfig(String topicUserEvents, String topicAdminEvents, String topicDashboardEvents, Map<String, Object> kafkaProducerProperties) {
        this.topicUserEvents = topicUserEvents;
        this.topicAdminEvents = topicAdminEvents;
        this.topicDashboardEvents = topicDashboardEvents;
        this.kafkaProducerProperties = kafkaProducerProperties;
    }

    public String getTopicUserEvents() {
        return topicUserEvents;
    }

    public String getTopicAdminEvents() {
        return topicAdminEvents;
    }

    public String getTopicDashboardEvents() {
        return topicDashboardEvents;
    }

    public Map<String, Object> getKafkaProducerProperties() {
        return kafkaProducerProperties;
    }
}
