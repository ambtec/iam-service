package net.ambtec.aim.keycloak.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.jboss.logging.Logger;
import org.keycloak.events.Event;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class KeycloakEventProcessor {

    private static final Logger LOG = Logger.getLogger(KeycloakEventProcessor.class);

    private final String topicUserEvents;
    private final String topicAdminEvents;
    private final String topicDashboardEvents;
    private final Producer<String, String> kafkaProducer;
    private final KeycloakSession session;
    private final ObjectMapper mapper;

    public KeycloakEventProcessor(ModuleConfig moduleConfig,
                                  KeycloakSession session,
                                  Producer<String, String> kafkaProducer) {
        this.topicUserEvents = moduleConfig.getTopicUserEvents();
        this.topicAdminEvents = moduleConfig.getTopicAdminEvents();
        this.topicDashboardEvents = moduleConfig.getTopicDashboardEvents();
        this.session = session;
        this.kafkaProducer = kafkaProducer;
        this.mapper = new ObjectMapper();
    }

    public void processUserEvent(Event event) {
        enhanceAndSendUserEvent(event);
        sendActiveSessionsEvent(event.getRealmId());
    }

    private void enhanceAndSendUserEvent(Event event) {
        EnhancedUserEvent enhancedUserEvent = new EnhancedUserEvent(event);

        RealmModel realm = session.realms().getRealm(event.getRealmId());
        final UserModel user = session.users().getUserById(realm, event.getUserId());
        enhancedUserEvent.setGroups(user.getGroupsStream()
                .map(GroupModel::getName)
                .collect(Collectors.toList()));
        enhancedUserEvent.setUsername(user.getUsername());

        sendToKafka(enhancedUserEvent, topicUserEvents);
    }

    public void processAdminEvent(AdminEvent event) {
        sendToKafka(event, topicAdminEvents);
    }

    private void sendActiveSessionsEvent(String realmId) {
        RealmModel realm = session.realms().getRealm(realmId);
        Map<String, Long> onlineClientStats = session.sessions().getActiveClientSessionStats(realm, false);
        LOG.warn("onlineClientStats: " + onlineClientStats);
        sendToKafka(new ActiveSessionsChangedEvent(realmId,
                onlineClientStats.values().stream().mapToLong(Long::longValue).sum()), topicDashboardEvents);
    }

    private void sendToKafka(Object payload, String topic) {
        try {
            produceEvent(mapper.writeValueAsString(payload), topic);
        } catch (InterruptedException | JsonProcessingException | TimeoutException | ExecutionException e) {
            LOG.error("error sending message", e);
        }
    }

    private void produceEvent(String eventAsString, String topic)
            throws InterruptedException, ExecutionException, TimeoutException {
        LOG.warn("Produce to topic: " + topic);
        LOG.warn("event: " + eventAsString);
        Future<RecordMetadata> metaData = kafkaProducer.send(new ProducerRecord<>(topic, eventAsString));
        RecordMetadata recordMetadata = metaData.get(30, TimeUnit.SECONDS);
        LOG.warn("Produced to topic: " + recordMetadata.topic());
    }
}
