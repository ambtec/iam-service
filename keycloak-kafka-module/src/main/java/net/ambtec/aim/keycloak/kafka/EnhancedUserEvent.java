package net.ambtec.aim.keycloak.kafka;

import org.keycloak.events.Event;

import java.util.List;

public class EnhancedUserEvent extends Event {

    private String username;
    private List<String> groups;

    public EnhancedUserEvent(Event event) {
        this.setId(event.getId());
        this.setTime(event.getTime());
        this.setType(event.getType());
        this.setRealmId(event.getRealmId());
        this.setClientId(event.getClientId());
        this.setUserId(event.getClientId());
        this.setSessionId(event.getSessionId());
        this.setIpAddress(event.getIpAddress());
        this.setError(event.getError());
        this.setDetails(event.getDetails());
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
