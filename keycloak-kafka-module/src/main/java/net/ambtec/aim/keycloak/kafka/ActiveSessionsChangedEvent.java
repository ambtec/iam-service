package net.ambtec.aim.keycloak.kafka;

public class ActiveSessionsChangedEvent {
    private String realmId;
    private long activeSessions;

    public ActiveSessionsChangedEvent(String realmId, long activeSessions) {
        this.realmId = realmId;
        this.activeSessions = activeSessions;
    }

    public String getRealmId() {
        return realmId;
    }

    public long getActiveSessions() {
        return activeSessions;
    }
}
