package net.ambtec.aim.keycloak.kafka;

import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class KafkaEventListenerProviderFactory implements EventListenerProviderFactory {

    private static final Logger LOG = Logger.getLogger(KafkaEventListenerProviderFactory.class);
    private static final String MODULE_ID = "kafka";

    private ModuleConfig moduleConfig;
    private KafkaEventListenerProvider instance;

    @Override
    public EventListenerProvider create(KeycloakSession session) {
        LOG.info("create event listener provider ...");
        if (instance == null) {
            instance = new KafkaEventListenerProvider(moduleConfig, session);
        }
        return instance;
    }

    @Override
    public String getId() {
        return MODULE_ID;
    }

    @Override
    public void init(Scope config) {
        LOG.info("Init kafka module ...");
        moduleConfig = new ModuleConfig(
                getConfig(config, "topicUserEvents"),
                getConfig(config, "topicAdminEvents"),
                getConfig(config, "topicDashboardEvents"),
                KafkaProducerFactory.init(config));
    }

    private String getConfig(Scope config, String name) {
        return getConfig(config, name, "???");
    }

    private String getConfig(Scope config, String name, String defaultValue) {
        String value = config.get(name, defaultValue);
        LOG.info(String.format("%s = %s", name, value));
        return value;
    }

    @Override
    public void postInit(KeycloakSessionFactory arg0) {
        // ignore
    }

    @Override
    public void close() {
        // ignore
    }
}
