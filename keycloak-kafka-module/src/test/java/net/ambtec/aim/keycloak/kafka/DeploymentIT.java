package net.ambtec.aim.keycloak.kafka;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import org.junit.jupiter.api.Test;

public class DeploymentIT {

    @Test
    public void shouldStartKeycloakWithNonExistingExtensionClassFolder() {
        try (KeycloakContainer keycloak = new KeycloakContainer()
                .withExtensionClassesFrom("target/does_not_exist")) {
            keycloak.start();
        }
    }
}
