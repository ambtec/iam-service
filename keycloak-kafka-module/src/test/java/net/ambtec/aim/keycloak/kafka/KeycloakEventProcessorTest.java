package net.ambtec.aim.keycloak.kafka;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.events.Event;
import org.keycloak.models.*;
import org.keycloak.models.utils.ReadOnlyUserModelDelegate;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class KeycloakEventProcessorTest {

    KeycloakEventProcessor processor;

    @Mock
    ModuleConfig moduleConfig;

    @Mock
    KeycloakSession keycloakSession;

    @Mock
    Producer<String, String> producer;

    @BeforeEach
    void setUp() {
        processor = new KeycloakEventProcessor(TestUtils.createModuleConfig(),
                keycloakSession,
                producer);
    }

    @Test
    @Disabled
    void processUserEvent() {
        final Event event = new Event();
        event.setUserId("userId");
        final CompletableFuture<RecordMetadata> future = new CompletableFuture<>();
        future.complete(new RecordMetadata(new TopicPartition("testTopic", 0),
                0,
                0,
                0,
                0L, 0, 0));
        final UserProvider userProvider = mock(UserProvider.class);
        final UserModel userModel = mock(UserModel.class);
        final GroupModel groupModel = mock(GroupModel.class);

        when(producer.send(any())).thenReturn(future);
        when(keycloakSession.realms()).thenReturn(mock(RealmProvider.class));
        when(keycloakSession.users()).thenReturn(userProvider);
        when(groupModel.getName()).thenReturn("groupName");
        when(userModel.getGroupsStream()).thenReturn(Stream.<GroupModel>builder().add(groupModel).build());
        when(userProvider.getUserById(any(), eq("userId"))).thenReturn(userModel);
        when(keycloakSession.sessions()).thenReturn(mock(UserSessionProvider.class));

        processor.processUserEvent(event);

        verify(producer, times(2)).send(any());
    }

    @Test
    void processAdminEvent() {
    }
}
