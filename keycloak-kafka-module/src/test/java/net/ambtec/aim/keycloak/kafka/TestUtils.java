package net.ambtec.aim.keycloak.kafka;

public class TestUtils {

    public static ModuleConfig createModuleConfig() {
        return new ModuleConfig("topicUserEvents",
                "topicAdminEvents",
                "topicDashboardEvents",
                null);
    }
}
