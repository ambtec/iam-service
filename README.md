<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# IAM Service

## Frontend Login Flow with IAM-Connector and Socket support

The Frontend login flow chart visualize the request flow: \
![Communication Flow](./assets/IAM-Service.drawio.png "Communication Flow") \
(Made with https://app.diagrams.net/)

- 1: Initial Authentication request from client
- 2: On success IAM response to client
- 3: On success new event of type 'LOGIN' is send to Event Broker
- 4: Socket Service consume event 'LOGIN' and register the client based on IAM payload
- 5: Client ask to register for socket stream. Socket Service request client to authenticate. A IAM identification is expected. It is compared with the registered data and on a successfull match client is granted access to socket stream.

## Published Events

see [asyncapi-doc](api-doc/asyncapi.md)

## Startup within Project Infrastructure Example

Clone repository [Project Infrastructure Example](https://gitlab.com/ambtec/infrastructure-example) and navigate to folder `` infrastructure-example ``.

To startup run `` sh up.sh `` on your Shell. This helper script will prepare the ENV configuration and start up the docker setup for you.

On your first startup you will be prompt to enter a IAM username and password. This is a one time action as long as the ENV exist. The credentials will be your Admin user to login to the IAM admin backend.

Press `` ctrl `` + `` c `` to shut down the running docker services.

You can run `` sh clean.sh `` on your Shell to clean the system. Be aware that you will be asked if you like to prune images and volumes. This step is optional but will make sure to prevent conflicts with cached data.

## Local start without Project Infrastructure Example

Run `` docker-compose up `` to start and navigate to `` http://127.0.0.1:28080/auth/ ``.

On first start you can't do much unless you have created a local admin account. Run `` docker exec iam-service /opt/jboss/keycloak/bin/add-user-keycloak.sh -u username -p password && docker restart iam-service `` in a new console window and wait, till the service restart is done.

After you can login with Username `` username `` and Password `` password ``.

If you encounter a `` port is already allocated `` error use `` netstat -aon `` to list all processes and used port and kill the process blocking this port. A Docker restart might work, too.

## ENV parameters

- `` IAM_USER ``: The choosen admin username
- `` IAM_PASS ``: The choosen admin password
- `` IAM_DB ``: The IAM database
- `` IAM_SERVICE_HOST ``: The container host
- `` IAM_DEFAULT_CLIENT_ID ``: The IAM default client id. Example: account
- `` IAM_ADDRESS_TOKEN ``: The IAM token url. Example: /auth/realms/ambtec/protocol/openid-connect/token
- `` IAM_ADDRESS_LOGOUT ``: The IAM logout url. Example: /auth/realms/ambtec/protocol/openid-connect/logout

## Login as Admin on localhost

Goto http://localhost:28080 and select `` Administration Console > ``

![IAM admin index](./assets/IAM-Admin-Index.png "IAM admin index") \
(1. IAM admin index)

This will lead you to the admin login screen where you can use the admin credentials you have choosen to login.

![IAM admin login](./assets/IAM-Admin-Login.png "IAM admin login") \
(2. IAM admin login)

On login success you will be redirected to the admin control center.

### Register a new user on localhost

There a two ways to register a new user via the user registration form or via the admin control center.

#### Add a new user via register form

If you, for example, use the [Dashboard Widget](https://gitlab.com/ambtec/dashboard-widget) you will be redirect to the IAM login form first.

![IAM user login](./assets/IAM-User-Login.png "IAM user login") \
(3. IAM user login)

Here you can use 'New user? Register' to go to the register form.

![IAM user registration](./assets/IAM-User-Registration.png "IAM user registration") \
(4. IAM user registration)

Use this form to registrate a new user.

*Notice*: The user activation does not work because no email is send in the current setup on your localhost. There for you have to go to the admin control center to activate the user manually. See 'Add a new user via admin control center' for more information.

#### Add a new user via admin control center

Logged in to the admin control center you'll see user management on the left side.

![IAM user management](./assets/IAM-Admin-Usermanagement.png "IAM user management") \
(5. IAM user management)

Choose `` Users `` to open the user manager.

![IAM user manager](./assets/IAM-Admin-Usermanager.png "IAM user manager") \
(6. IAM user manager)

If you have a already created user account use the search to find it and select `` Edit `` or choose `` Add user `` to created a new one.

Both options will open the user editor.

![IAM user editor](./assets/IAM-Admin-Add-User.png "IAM user editor") \
(7. IAM user editor)

*Notice*: On a new user creation event you'll have to fill out and save the user first. Only then you are allowed to see the other tabs for manage password, groups, etc.

If you are in a already existing user account you can enable it via the `` User Enabled `` flag.

#### Manage user credentials

To manage the user password go to `` Credentials `` tab.

![IAM user password](./assets/IAM-Admin-User-Password.png "IAM user password") \
(8. IAM user password)

If you do, choose a new password and press `` save ``.

#### Manage user groups

To manage user groups go to `` Groups `` tab. The user groups grant access to certain features of the project.

![IAM user groups](./assets/IAM-Admin-User-Groups.png "IAM user groups") \
(9. IAM user groups)

Select one and press `` Join `` to add the selected group to the user account.

#### Login with your user
You have no a working user account. Go to your frontend login form like on [Dashboard Widget](https://gitlab.com/ambtec/dashboard-widget) and login with that user.
## License
MIT

## Credits
- https://www.keycloak.org/
- https://github.com/keycloak/keycloak-containers

